#!/bin/bash

SRC_FILE=bashrc-ext.sh
DST_FILE=.bashrc-ext

# If not installed yet - copy it

cp $(dirname $0)/${SRC_FILE} ~/${DST_FILE}

# If not installed the bashrc-ext sourcing - add it into ~/.bashrc

grep bashrc-ext ~/.bashrc > /dev/null

if [ $? -ne 0 ]
then
	echo "" >> ~/.bashrc
	echo "source ~/${DST_FILE}"  >> ~/.bashrc
fi 

source ~/.bashrc

echo "All done"
