# Sebastian Mountaniol
# Copyleft: GPL
# This is alias extention for .bashrc file
#
# This project replaces long and compilcated command (like 'find')
# with simple one- or two characters aliases, and wraps necessity to
# type all the arguments.
# For example: 
# "find ./ -name PATTERN -exec file '{}' \;"
# calls with "s PATTERN ./ file"

# DEFINITIONS

# what is the text editor? "vi" by default
TEXT_EDITOR="vi"

# What type of files to exclude from grepping?
GREP_EXCLUDE=""

# What patterns to exlude from "find"?
FIND_EXCLUDE=""

# Colors

yellow="\\033[33m"
red="\\033[31m"
back_black="\\033[40m"
bold="\\033[1m"


# sw alias: Find a file recursively from current dir and open every found in text editor
function __do_find_work()
{
        local pattern=$1
        local action=$2

        local f_str="./ "

        f_str+=" -name $pattern -exec ${TEXT_EDITOR} {} ;"

        find $f_str
}

# s alias: search (same as "find")
function __do_find()
{
        local pattern=$1
        local path=$2
        local action=$3

        local f_str=""

        [[ "${path}" != "" ]] &&  f_str+="$path "

        f_str+=" -name $pattern"

        [[ "${action}" != "" ]] && f_str+=" -exec ${action} {} ;"

        echo "/ ${f_str} /"
        find $f_str
}

# This is addition to the stabdard .bashrc
# It implements several comples aliases, see below
# .bashrc.ext

function bash_help()
{
        echo "Extended aliases:"
        echo "=========================="
        echo "[01] ll         | detailed list"
        echo "[02] g pattern  | grep recursively, exclude o.cmd files; examples: (1) 'g VARIABLE' (2) 'g VAR ~/src/'"
        echo "[03] gg pattern | show files that contain pattern, using 'g' command"
        echo "[04] x pattern  | find a pattern using global -x"
        echo "[05] f pattern  | much faster than grep; find files l"
        echo "[06] ff pattern | much faster than grep; show files that contain pattern; add '-c' at the end to see count"
        echo "[07] sw file    | search & work: find a file 'file' and open it in text edior (vi by default, see 'hh'"
        echo "[08] mmake      | run make, color errors and warnings"
        echo "[09] gl         | run visual git directory diff of 'comm' and 'comm'~1 ; use HEAD if no 'comm' is given"
	echo "[10] gll        | run directory based visual diff of the 'comm1' and 'comm2'"
        echo "[11] h          | show this help"
        echo "[12] hh         | show detailed help"
        echo "-----------------------------------------------------------------------------------------------"
}

function bash_help_detailed()
{
        echo "Extended aliases, full help:"
        echo "-----------------------------------------------------------------------------------------------"
        echo "ll [dir] :"
        echo "   detailed list of a directory"
        echo "-----------------------------------------------------------------------------------------------"
        echo "g pattern [dir] :"
        echo "   grep recursively, exclude o.cmd files"
        echo "   if [dir] given, search under this dir"
        echo "   if [dir] not given, search from the current dir"
        echo "-----------------------------------------------------------------------------------------------"
        echo "gg pattern [dir] :"
        echo "   show files that contain pattern, using 'g' command"
        echo "   if [dir] given, search from this dir"
        echo "   if [dir] not given, search from the current dir"
        echo "-----------------------------------------------------------------------------------------------"
        echo "x  pattern :"
        echo "   find a pattern using command 'global -x', see 'gtags'"
        echo "-----------------------------------------------------------------------------------------------"
        echo "f  pattern [dir] :"
        echo "   this is a faster version of grep, combines find + paralleled grep"
        echo "   It searches using 'find' + 'grep' pattern in all files recursively; it always exclude '*.o' and '*.cmd' files"
        echo "   if [dir] is given, then search from [dir]"
        echo "   if [dir] not given, then search from the current dir"
        echo "-----------------------------------------------------------------------------------------------"
        echo "ff pattern [dir] [-c] :"
        echo "   shows sorted set of files (no file name duplicated) which contain pattern, using 'f' command"
        echo "   if [dir] is given, then search from [dir]"
        echo "   if [dir] not given, then search from the current dir"
        echo "   add '-c' to see count of entries of pattern per file"
        echo "-----------------------------------------------------------------------------------------------"
        echo "sw file_name"
        echo "   Find the file file_name and open in in vi for editing"
	echo "   sw filen_name executes 'find -name file_name -exec \${TEXT_EDITOR} {} ;'"
	echo "   You may redefine the text editor in the beginning of bashrc-ext"
        echo "-----------------------------------------------------------------------------------------------"
        echo "mmake - run make file and color errors and warnings "
        echo "-----------------------------------------------------------------------------------------------"
	echo "gl commit - run directory based visual diff of the 'commit' and 'commit'~1 ; use HEAD if no commit num is given"
        echo "-----------------------------------------------------------------------------------------------"
	echo "gll comm1 comm2   - run directory based visual diff of the 'comm1' znd 'comm2'"
        echo "-----------------------------------------------------------------------------------------------"
        echo "h  - show short help"
        echo "-----------------------------------------------------------------------------------------------"
        echo "hh - show [this] detailed help"
}


# alais f: fast find using 'find' command + parallel grep
# If the last argument is a dir, we search in this dir;
# otherwise, search in the current dir
function _f()
{
        local _pattern="$@"
	# The directory we search in:
	# Set it to be the last argument;
	# If we'll find later, that it is not a dir, we will reset it 
       	local _dir="${@: -1}"

	# If the last argument is directory, it means we search in this directory
        if [[ -d "$_dir" ]]
	then
		# If the last argument is a directory name, we remove it from the arguments' list
		# echo "The last argument is a dir"
		# Remove the last argument form the arguments list
		set -- "${@:1:$(($#-1))}"
		# Reset the pattern
		_pattern="$@"
	else
		# echo "The last argument is NOT a dir"
		# If the last argument is not directory, we search in the current dir
		_dir="./"
	fi
        #[[ ! -d "$_dir" ]] && _dir="./"

	#echo find ${_dir} ! -name "*.cmd" ! -name "*.o"
	#echo xargs -P $(nproc) grep -nsI "${_pattern}"
	echo sed -E -e "s/"${_pattern}"/$(echo -e "${red}${bold}"${_pattern}"\\033[0m"/g)"

        find ${_dir} ! -name "*.cmd" ! -name "*.o"  | xargs -I {} -P $(nproc) grep -rnsI "${_pattern}" {} | sed -E -e "s/"${_pattern}"/$(echo -e "${red}${bold}"${_pattern}"\\033[0m"/g)"
        #find ${_dir} ! -name "*.cmd" ! -name "*.o"  | xargs -P $(nproc) grep -rns "${_pattern}"
}

# alias ff: find all files contain given pattern
function _f_files()
{
        local pat=$1
        local dir=$2
        local count=$3

        # There are 4 scenarios of "ff" command usage:
        # 1. ff PATTERN
        # 2. ff PATTERN DIR
        # 3. ff PATTERN DIR -c
        # 4. ff PATTERN -c

        # Here we care about scenarion 4:
        # in this case $dir = "-c" which it illigal

        if [ "$dir" = "-c" ]
        then
                dir="./"
                count="-c"
        fi

	local _F=$(_f $pat $dir | awk -F ':' '{print $1}')
	for ff in ${_F} ; do [[ -f ${ff} ]] && echo ${ff} ; done | sort | uniq $count

       # _f $pat $dir | awk -F ':' '{print $1}' | sort | uniq $count
}

function _g_files()
{
        local pat=$1
        local dir=$2
        local count=$3

        # There are 4 scenarios of "ff" command usage:
        # 1. ff PATTERN
        # 2. ff PATTERN DIR
        # 3. ff PATTERN DIR -c
        # 4. ff PATTERN -c

        # Here we care about scenarion 4:
        # in this case $dir = "-c" which it illigal

        if [ "$dir" = "-c" ]
        then
                dir="./"
                count="-c"
        fi

        grep -rnsI $pat --exclude='*.o.cmd' $dir | awk -F ':' '{print $1}' | sort | uniq $count
}

function __mmake()
{
	/usr/bin/make "$@" 2>&1 | sed -E -e \
	"s/[Ee]rror/$(echo -e "${red}${bold}${back_black}"ERROR"\\033[0m"/g)" -e \
	"s/undefined reference/$(echo -e "${bold}${yellow}${back_black}"undefined reference"\\033[0m"/g)" -e \
	"s/undefined!/$(echo -e "${bold}${red}${back_black}"undefined!"\\033[0m"/gI)" -e \
	"s/[Ww]arning/$(echo -e "${yellow}${bold}${back_black}"WARNING"\\033[0m"/gI)"
return ${PIPESTATUS[0]}
}

# This function accepts one optional arg: hash of comment.
# If this function run without arg, it assumes that it is HEAD
function __git_dir_diff_of_commit()
{
	local commit=$1
	commit=${commit:="HEAD"}
	echo "commit == ${commit}"
	git difftool --dir-diff ${commit}..${commit}~1
}

# This function run visual diff of two commits, given as arguments
function __git_dir_diff_two_commits()
{
	local commit1=$1
	local commit2=$2
	echo "commits = ${commit1}..${commit2}"
	git difftool --dir-diff ${commit1}..${commit2}
}
alias g='grep -rnsI --exclude="*.o.cmd"'
alias gg='_g_files'

alias f='_f'

alias ff='_f_files'
alias x='global -x'
#alias killssh='for p in $(ps ax | grep seb@dev106 | grep -v grep | awk \\'{print $1}\\') ; do kill -9 $p ; done'
alias killssh='for p in $(ps ax | grep seb@dev106 | grep -v grep | cut -d " " -f2  ) ; do kill -9 $p ; done'

############################################################################
# Alias 's' is for 'find'
# Usage: 
# find DIR -name PATTERN -> s PATTERN DIR
# find ./ -name PATTERN  -> s PATTERN
# find DIR PATTERN -exec ACTION '{}' \; -> s PATTERN DIR ACTION
# Example:
# Find all files match "leroy*" and run "file" command for each:
# s leroy* file
# The same for with find command:
# find ./ -name "leroy*" -exec file '{}' \;
############################################################################

alias s=__do_find

# Find file and open in editor
# sw = "search & work"
alias sw=__do_find_work

alias h='bash_help'
alias hh='bash_help_detailed'
alias mmake="__mmake"

alias ll='ls -la'
alias bb='bcompare'
#alias glast='git difftool --dir-diff HEAD..HEAD~1'
alias gl='__git_dir_diff_of_commit'
alias gll='__git_dir_diff_two_commits'

# Add GIT branch into prompt
function __git_ps1() {
	git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

function __dir_ps1()
{
	pwd | awk -F '/' '{print $(NF-1)"/"$NF}'
}

function pcommand()
{
	local dir=$(__git_dir)
	local git_b=$(__dir_ps1)
	
}
#PROMPT_COMMAND=pcommand
#export PS1="\\w:\$(git branch 2>/dev/null | grep '^*' | colrm 1 2)\$ "
#export PS1="\\u:[$(__git_dir)]$(__git_ps1) \$ "
# export PS1="\u:\W \\$\[$(__git_ps1) $(tput sgr0)\] "

#export PS1="\\n\[\033[38;5;246m\]\u@\[\033[38;5;245m\]\h\[\033[38;5;15m\] \[\033[38;5;28m\]\w\[\033[38;5;15m\]\[\033[38;5;2m\]$(__git_ps1)$ "
#export PS1="…`__git_ps1`…"

function prompt_command () {
    if [ "\$(type -t __git_ps1)" ]; then # if we're in a Git repo, show current branch
        BRANCH=$(__git_ps1 '[ %s ] ')
    fi
    local TITLEBAR=$(pwdtail)
#    export PS1="\\u:\[${TITLEBAR}\]\w${BRANCH}${DEFAULT}$ "
    export PS1="\\u@[$TITLEBAR]${BRANCH} \$ "
}

PROMPT_COMMAND=prompt_command

function pwdtail () { #returns the last 2 fields of the working directory
    pwd|awk -F/ '{nlast = NF -1;print $nlast"/"$NF}'
}

