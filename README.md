# bashrc_ext

This project introduces a set of aliases for people who works extensively with BASH
and performs a lot of grepping, searching and so.

Instead of typing 'grep -rnsI' of 'find ./ -name ...' it introduces simple aliases.
Part of these aliases work even more effectively than original commands.

Some aliases are a simple form for a chain of commands.

For example, the 'gg PATTERN' alias is:

grep -rnsI PATTERN | sort | uniq

Here's a short list of aliases and explanation.

* h - show a short help
* hh - show help with examples and explanation s - a short form of 'find'
* sw - it is "search and work" alias: search files and open every found in test editor
* g - a short form of 'grep -rnsI'
* gg - grep recursively for a pattern, and show sorted list of files where this pattern present
* f - a faster paralleled grep command, works 3-10 times faster than a regular recursive grep
* ff - the same as 'gg' but works 3-10 times faster (parelleled search)

